﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

namespace GameOfLife
{
    class Grid
    {
        public Point Size { get; private set; }

        private Cell[,] cells;
        private bool[,] nextCellStates;

        private TimeSpan updateTimer;

        public Grid()
        {
            Size = new Point(Game1.CellsX, Game1.CellsY);

            cells = new Cell[Size.X, Size.Y];
            nextCellStates = new bool[Size.X, Size.Y];

            for (int i = 0; i < Size.X; i++)
            {
                for (int j = 0; j < Size.Y; j++)
                {
                    cells[i, j] = new Cell(new Point(i, j));
                    nextCellStates[i, j] = false;
                }
            }

            updateTimer = TimeSpan.Zero;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            foreach (Cell cell in cells)
                cell.Draw(spriteBatch);

            // Vetical grid lines
            for (int i = 0; i < Size.X; i++)
                spriteBatch.Draw(Game1.Pixel, new Rectangle(i * Game1.CellSize - 1, 0, 1, Size.Y * Game1.CellSize), Color.DarkGray);

            // Horizontal grid lines
            for (int j = 0; j < Size.Y; j++)
                spriteBatch.Draw(Game1.Pixel, new Rectangle(0, j * Game1.CellSize - 1, Size.X * Game1.CellSize, 1), Color.DarkGray);
        }

        public void Update(GameTime gameTime)
        {
            MouseState mouseState = Mouse.GetState();

            foreach (Cell cell in cells)
                cell.Update(mouseState);

            if (Game1.Paused)
                return;

            updateTimer += gameTime.ElapsedGameTime;

            if (updateTimer.TotalMilliseconds > 1000f / Game1.UPS)
            {
                updateTimer = TimeSpan.Zero;

                // Loop through ever cell on the grid
                for (int i = 0; i < Size.X; i++)
                {
                    for (int j = 0; j < Size.Y; j++)
                    {
                        // Check the cell's state, count its ALIVE neighbors
                        bool living = cells[i, j].IsAlive;
                        int count = GetLivingNeighbors(i, j);
                        bool result = false;

                        // Apply rules and set the next state
                        if (living && count < 2)
                            result = false;
                        if (living && (count == 2 || count == 3))
                            result = true;
                        if (living && count > 3)
                            result = false;
                        if (!living && count == 3)
                            result = true;

                        nextCellStates[i, j] = result;
                    }
                }
                SetNextState();
            }
        }

        public void Clear()
        {
            for (int i = 0; i < Size.X; i++)
                for (int j = 0; j < Size.Y; j++)
                    nextCellStates[i, j] = false;

            SetNextState();
        }

        /**
         * Acquires all the current living cells around target cell.
         * 
         * @param x The X coordinate of the target cell.
         * @param y The Y coordinate of the target cell.
         * @return int The number of alive cells around the target cell.
         */
        public int GetLivingNeighbors(int x, int y)
        {
            int count = 0;

            // Check cell on the right
            if (x != Size.X - 1)
                if (cells[x + 1, y].IsAlive)
                    count++;

            // Check cell on bottom right
            if (x != Size.X - 1 && y != Size.Y - 1)
                if (cells[x + 1, y + 1].IsAlive)
                    count++;

            // Check cell on botttom
            if (y != Size.Y - 1)
                if (cells[x, y + 1].IsAlive)
                    count++;

            // Check cell on the bottom left.
            if (x != 0 && y != Size.Y - 1)
                if (cells[x - 1, y + 1].IsAlive)
                    count++;

            // Check cell on the left.
            if (x != 0)
                if (cells[x - 1, y].IsAlive)
                    count++;

            // Check cell on the top left.
            if (x != 0 && y != 0)
                if (cells[x - 1, y - 1].IsAlive)
                    count++;

            // Check cell on the top.
            if (y != 0)
                if (cells[x, y - 1].IsAlive)
                    count++;

            // Check cell on the top right.
            if (x != Size.X - 1 && y != 0)
                if (cells[x + 1, y - 1].IsAlive)
                    count++;

            return count;
        }

        public void SetNextState()
        {
            for (int i = 0; i < Size.X; i++)
                for (int j = 0; j < Size.Y; j++)
                    cells[i, j].IsAlive = nextCellStates[i, j];
        }

        public void GenerateCells()
        {
            this.Clear();

            Game1.Paused = true;

            Random generator = new Random();
            int numOfCells = generator.Next(500, 4500);

            for (int i = 0; i < numOfCells; i++)
            {
                int randomX = generator.Next(Game1.CellsX);
                int randomY = generator.Next(Game1.CellsY);
                if (cells[randomX, randomY].IsAlive)
                    i = i - 1;
                else 
                    cells[randomX, randomY].IsAlive = true;
            }
        }

        public void CreateGosperGliderGun()
        {
            this.Clear();

            Game1.Paused = true;

            // First square
            cells[10, 10].IsAlive = true;
            cells[11, 10].IsAlive = true;
            cells[10, 11].IsAlive = true;
            cells[11, 11].IsAlive = true;

            // First ship
            cells[20, 10].IsAlive = true;
            cells[20, 11].IsAlive = true;
            cells[20, 12].IsAlive = true;
            cells[21, 9].IsAlive = true;
            cells[21, 13].IsAlive = true;
            cells[22, 8].IsAlive = true;
            cells[23, 8].IsAlive = true;
            cells[22, 14].IsAlive = true;
            cells[23, 14].IsAlive = true;
            cells[24, 11].IsAlive = true;
            cells[25, 13].IsAlive = true;
            cells[25, 9].IsAlive = true;
            cells[26, 12].IsAlive = true;
            cells[26, 11].IsAlive = true;
            cells[26, 10].IsAlive = true;
            cells[27, 11].IsAlive = true;

            // Second ship
            cells[30, 10].IsAlive = true;
            cells[30, 9].IsAlive = true;
            cells[30, 8].IsAlive = true;
            cells[31, 10].IsAlive = true;
            cells[31, 9].IsAlive = true;
            cells[31, 8].IsAlive = true;
            cells[32, 11].IsAlive = true;
            cells[32, 7].IsAlive = true;
            cells[34, 7].IsAlive = true;
            cells[34, 6].IsAlive = true;
            cells[34, 11].IsAlive = true;
            cells[34, 12].IsAlive = true;

            // Second square
            cells[44, 9].IsAlive = true;
            cells[44, 8].IsAlive = true;
            cells[45, 9].IsAlive = true;
            cells[45, 8].IsAlive = true;
        }
    }
}
