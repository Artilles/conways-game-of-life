using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace GameOfLife
{
    /// <summary>
    /// Conway's Game of Life
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        public const int UPS = 20; // Updates per second
        public const int FPS = 60;

        public const int CellSize = 10; // Cell pixel width/height
        public const int CellsX = 100;
        public const int CellsY = 50;

        public static bool Paused = true;

        public static SpriteFont Font;
        public static Texture2D Pixel;

        public static Vector2 ScreenSize;

        private Grid grid;

        private KeyboardState keyboardState, lastKeyboardState;

        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            IsFixedTimeStep = true;
            TargetElapsedTime = TimeSpan.FromSeconds(1.0 / UPS);

            ScreenSize = new Vector2(CellsX, CellsY) * CellSize;

            graphics.PreferredBackBufferWidth = (int)ScreenSize.X;
            graphics.PreferredBackBufferHeight = (int)ScreenSize.Y;

            IsMouseVisible = true;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            base.Initialize();

            grid = new Grid();

            keyboardState = lastKeyboardState = Keyboard.GetState();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            Font = Content.Load<SpriteFont>("Font");

            Pixel = new Texture2D(spriteBatch.GraphicsDevice, 1, 1);
            Pixel.SetData(new[] { Color.White });
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            keyboardState = Keyboard.GetState();

            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            // Toggle pause when spacebar is pressed.
            if (keyboardState.IsKeyDown(Keys.Space) && lastKeyboardState.IsKeyUp(Keys.Space))
                Paused = !Paused;

            // Clear the screen if backspace is pressed.
            if (keyboardState.IsKeyDown(Keys.Back) && lastKeyboardState.IsKeyUp(Keys.Back))
                grid.Clear();

            // Randomly generate a bunch of cells when enter is pressed.
            if (keyboardState.IsKeyDown(Keys.Enter) && lastKeyboardState.IsKeyUp(Keys.Enter))
                grid.GenerateCells();

            if (keyboardState.IsKeyDown(Keys.Q) && lastKeyboardState.IsKeyUp(Keys.Q))
                grid.CreateGosperGliderGun();

            base.Update(gameTime);

            grid.Update(gameTime);

            lastKeyboardState = keyboardState;
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            if (Paused)
                GraphicsDevice.Clear(Color.Red);
            else
                GraphicsDevice.Clear(Color.DarkGray);

            spriteBatch.Begin();
            
            // Draw the title (neeeds to be moved up)
            string title = "Game of Life";
            // Set title to float at top of screen
            Vector2 size = Font.MeasureString(title);
            Vector2 origin = size * 0.5f;
            origin.Y += (ScreenSize.Y) - (size.Y / 2);

            grid.Draw(spriteBatch);

            if (Paused)
            {
                string paused = "Paused";
                spriteBatch.DrawString(Font, paused, ScreenSize / 2, Color.Gray, 0f, Font.MeasureString(paused) / 2, 1f, SpriteEffects.None, 0f);
            }

            spriteBatch.DrawString(Font, title, ScreenSize / 2, Color.Black, 0f, origin, 0.5f, SpriteEffects.None, 0f);

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
