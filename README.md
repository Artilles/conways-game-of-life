GameOfLife
==========

Language: C# <br/>
Platform: XNA <br/>
<br/>
Zachary Bell's work on a Conway's Game of Life simulator.<br/>


Key Commands
============
Space     - Pause/Unpause <br/>
Enter     - Generate random cells <br/>
Backspace - Clear all cells <br/>
Q         - Create a Gosper Glider Gun <br/>
